//
//  IDPicker.m
//  DTMF
//
//  Created by NguyenTranHoangNam on 8/12/14.
//  Copyright (c) 2014 ApproMobile. All rights reserved.
//

#import "IDPicker.h"
#import "Setting.h"
@implementation IDPicker

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    // Initialization code
  }
  return self;
}

- (void)refresh {
  int lightID = 1;
  switch (self.tab) {
  case 1: {
    lightID = [SETTING getLightID];
    break;
  }
  case 2: {
    lightID = [SETTING getConsentID];
    break;
  }
  default:
    break;
  }
  [self.btID1 setImage:[UIImage imageNamed:@"radio_uncheck"]
              forState:UIControlStateNormal];
  [self.btID2 setImage:[UIImage imageNamed:@"radio_uncheck"]
              forState:UIControlStateNormal];
  [self.btID3 setImage:[UIImage imageNamed:@"radio_uncheck"]
              forState:UIControlStateNormal];
  [self.btID4 setImage:[UIImage imageNamed:@"radio_uncheck"]
              forState:UIControlStateNormal];
  switch (lightID) {
  case 1: {
    [self.btID1 setImage:[UIImage imageNamed:@"radio_check"]
                forState:UIControlStateNormal];
    break;
  }
  case 2: {
    [self.btID2 setImage:[UIImage imageNamed:@"radio_check"]
                forState:UIControlStateNormal];
    break;
  }
  case 3: {
    [self.btID3 setImage:[UIImage imageNamed:@"radio_check"]
                forState:UIControlStateNormal];
    break;
  }
  case 4: {
    [self.btID4 setImage:[UIImage imageNamed:@"radio_check"]
                forState:UIControlStateNormal];
    break;
  }
  default:
    break;
  }
}

- (IBAction)pickID:(UIButton *)sender {
  switch (self.tab) {
  case 1: {
    [SETTING saveLightID:sender.tag];
    break;
  }
  case 2: {
    [SETTING saveConsentID:sender.tag];
    break;
  }
  default:
    break;
  }
  [self refresh];
  [self.delegate closeIDPicker];
}

- (IBAction)close {
  [self refresh];
  [self.delegate closeIDPicker];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
