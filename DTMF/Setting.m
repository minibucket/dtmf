//
//  Setting.m
//  DTMF
//
//  Created by NguyenTranHoangNam on 8/12/14.
//  Copyright (c) 2014 ApproMobile. All rights reserved.
//

#import "Setting.h"

@implementation Setting
+ (id)sharedInstance {
  static Setting *sharedInstance = nil;
  if (nil != sharedInstance) {
    return sharedInstance;
  }
  static dispatch_once_t pred;
  dispatch_once(&pred, ^{
      sharedInstance = [[Setting alloc] init];
      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
      if (![defaults objectForKey:@"LightID"]) {
        [defaults setObject:@"1" forKey:@"LightID"];
        [defaults setObject:@"1" forKey:@"ConsentID"];
        [defaults setObject:@"50" forKey:@"Volume1"];
        [defaults setObject:@"50" forKey:@"Volume2"];
      }
  });
  return sharedInstance;
}

- (void)saveLightID:(int)ID {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setObject:[NSString stringWithFormat:@"%i", ID] forKey:@"LightID"];
  [defaults synchronize];
}

- (int)getLightID {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  return [[defaults valueForKey:@"LightID"] intValue];
}

- (void)saveConsentID:(int)ID {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setObject:[NSString stringWithFormat:@"%i", ID]
               forKey:@"ConsentID"];
  [defaults synchronize];
}

- (int)getConsentID {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  return [[defaults valueForKey:@"ConsentID"] intValue];
}

- (void)saveVolume1:(int)volume {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setObject:[NSString stringWithFormat:@"%i", volume]
               forKey:@"Volume1"];
  [defaults synchronize];
}

- (int)getVolume1 {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  return [[defaults valueForKey:@"Volume1"] intValue];
}

- (void)saveVolume2:(int)volume {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setObject:[NSString stringWithFormat:@"%i", volume]
               forKey:@"Volume2"];
  [defaults synchronize];
}

- (int)getVolume2 {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  return [[defaults valueForKey:@"Volume2"] intValue];
}

- (NSString *)convertToSoundLight:(int)soundID {
  switch (soundID) {
  case 1:
    return @"A";
    break;
  case 2:
    return @"B";
    break;
  case 3:
    return @"C";
    break;
  case 4:
    return @"D";
    break;
  default:
    break;
  }
  return @"";
}

- (NSString *)convertToSoundConsent:(int)soundID {
    switch (soundID) {
        case 1:
            return @"D";
            break;
        case 2:
            return @"C";
            break;
        case 3:
            return @"B";
            break;
        case 4:
            return @"A";
            break;
        default:
            break;
    }
    return @"";
}
@end
