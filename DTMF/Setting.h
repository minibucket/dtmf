//
//  Setting.h
//  DTMF
//
//  Created by NguyenTranHoangNam on 8/12/14.
//  Copyright (c) 2014 ApproMobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#define SETTING [Setting sharedInstance]
@interface Setting : NSObject
+ (id)sharedInstance;
- (void)saveLightID:(int)ID;
- (int)getLightID;
- (void)saveConsentID:(int)ID;
- (int)getConsentID;
- (void)saveVolume1:(int)volume;
- (int)getVolume1;
- (void)saveVolume2:(int)volume;
- (int)getVolume2;
- (NSString *)convertToSoundLight:(int)soundID;
- (NSString *)convertToSoundConsent:(int)soundID;
@end
