//
//  LightControlVC.h
//  DTMF
//
//  Created by NguyenTranHoangNam on 8/12/14.
//  Copyright (c) 2014 ApproMobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IDPicker.h"
#import <AVFoundation/AVFoundation.h>
@interface LightControlVC
    : UIViewController <IDPickerDelegate, AVAudioPlayerDelegate> {
  BOOL isSetVolume;
  int currentPosition;
  NSString *currentButton;
  BOOL isPlaying;
}
@property(retain, nonatomic) IBOutlet UIButton *btID, *btVolume;
@property(retain, nonatomic) IBOutlet UISlider *volumeView;
@property IBOutlet UIView *background, *btBackground;
@property(retain, nonatomic) IDPicker *popup;
@property(nonatomic, retain) AVAudioPlayer *player;
- (void)disableVolume;
@end
