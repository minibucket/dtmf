//
//  AppDelegate.m
//  DTMF
//
//  Created by NguyenTranHoangNam on 8/12/14.
//  Copyright (c) 2014 ApproMobile. All rights reserved.
//

#import "AppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
#import "LightControlVC.h"
#import "ConsentVC.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  // Override point for customization after application launch.
  self.window.backgroundColor = [UIColor whiteColor];
  [self.window makeKeyAndVisible];
    UIStoryboard *storyBoard;

    if(!isiPhone5)
    storyBoard=  [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    else
         storyBoard=  [UIStoryboard storyboardWithName:@"Storyboard2" bundle:nil];
  UIViewController *initViewController =
      [storyBoard instantiateInitialViewController];
  [self.window setRootViewController:initViewController];
  MPMusicPlayerController *musicPlayer =
      [MPMusicPlayerController iPodMusicPlayer];

  oldVolume = musicPlayer.volume;

  musicPlayer.volume = 1;
  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
  // Sent when the application is about to move from active to inactive state.
  // This can occur for certain types of temporary interruptions (such as an
  // incoming phone call or SMS message) or when the user quits the application
  // and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down
  // OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
  MPMusicPlayerController *musicPlayer =
      [MPMusicPlayerController iPodMusicPlayer];
  musicPlayer.volume = oldVolume;

  ConsentVC *consentTab =
      (ConsentVC *)[((UITabBarController *)[self.window rootViewController])
                        .viewControllers objectAtIndex:1];
  [consentTab disableVolume];

  LightControlVC *lightTab = (LightControlVC *)
      [((UITabBarController *)[self.window rootViewController]).viewControllers
          objectAtIndex:0];
  [lightTab disableVolume];

  // Use this method to release shared resources, save user data, invalidate
  // timers, and store enough application state information to restore your
  // application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called
  // instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
  MPMusicPlayerController *musicPlayer =
      [MPMusicPlayerController iPodMusicPlayer];

  oldVolume = musicPlayer.volume;

  musicPlayer.volume = 1;

  // Called as part of the transition from the background to the inactive state;
  // here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {

  // Restart any tasks that were paused (or not yet started) while the
  // application was inactive. If the application was previously in the
  // background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
  // Called when the application is about to terminate. Save data if
  // appropriate. See also applicationDidEnterBackground:.
}

@end
