//
//  IDPicker.h
//  DTMF
//
//  Created by NguyenTranHoangNam on 8/12/14.
//  Copyright (c) 2014 ApproMobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IDPickerDelegate <NSObject>

- (void)closeIDPicker;

@end

@interface IDPicker : UIView
@property int tab;
@property IBOutlet UIButton *btID1, *btID2, *btID3, *btID4;
@property id<IDPickerDelegate> delegate;
- (void)refresh;
@end
